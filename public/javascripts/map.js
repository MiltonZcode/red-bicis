var map = L.map('main_map').setView([-34.852439942800686, -56.13693010816191], 16);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
}).addTo(map);


/*marker.bindPopup("<b>Duoc!</b><br>Sede PV.").openPopup();*/


$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici) {
            L.marker(bici.ubicacion, {title: bici.code}).addTo(map);
        });
    }
});